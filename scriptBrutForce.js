const fs = require('fs'); 
const readline = require('readline'); 

const HashCalcule = str => {
    let hash = str 
        .split("")
        .map((c, i) => str.charCodeAt(i)).map(c => c + 2)
        .map(c => String.fromCharCode(c)).join("");
    return Buffer.from(hash).toString("base64");
}

const rl = readline.createInterface({
    input: fs.createReadStream('hashText.txt')
});



rl.on('line', (line) => {
    const mot = HashCalcule(line);
    console.log(mot);
    if (mot == 'ZWpxZQ==') {
        console.log(line);
        return process.exit(1);
    }
})

rl.on('close', () => {
    console.log('done');
})

